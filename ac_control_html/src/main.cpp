/*
 * IRremoteESP8266: IRServer - demonstrates sending IR codes controlled from a webserver
 * Version 0.3 May, 2019
 * Version 0.2 June, 2017
 * Copyright 2015 Mark Szabo
 * Copyright 2019 David Conran
 *
 * An IR LED circuit *MUST* be connected to the ESP on a pin
 * as specified by kIrLed below.
 *
 * TL;DR: The IR LED needs to be driven by a transistor for a good result.
 *
 * Suggested circuit:
 *     https://github.com/crankyoldgit/IRremoteESP8266/wiki#ir-sending
 *
 * Common mistakes & tips:
 *   * Don't just connect the IR LED directly to the pin, it won't
 *     have enough current to drive the IR LED effectively.
 *   * Make sure you have the IR LED polarity correct.
 *     See: https://learn.sparkfun.com/tutorials/polarity/diode-and-led-polarity
 *   * Typical digital camera/phones can be used to see if the IR LED is flashed.
 *     Replace the IR LED with a normal LED if you don't have a digital camera
 *     when debugging.
 *   * Avoid using the following pins unless you really know what you are doing:
 *     * Pin 0/D3: Can interfere with the boot/program mode & support circuits.
 *     * Pin 1/TX/TXD0: Any serial transmissions from the ESP8266 will interfere.
 *     * Pin 3/RX/RXD0: Any serial transmissions to the ESP8266 will interfere.
 *   * ESP-01 modules are tricky. We suggest you use a module with more GPIOs
 *     for your first time. e.g. ESP-12 etc.
 */
#include <Arduino.h>
#if defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#endif  // ESP8266
#if defined(ESP32)
#include <WiFi.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#endif  // ESP32
//#include <IRremoteESP8266.h>
//#include <IRsend.h>
#include <WiFiClient.h>
#include <ir_Hitachi.h>
#include <string>
#include <iostream>
#include <main.h>
#include <WString.h>

MDNSResponder mdns;

#if defined(ESP8266)
ESP8266WebServer server(80);
#undef HOSTNAME
#define HOSTNAME "esp8266"
#endif  // ESP8266
#if defined(ESP32)
WebServer server(80);
#undef HOSTNAME
#define HOSTNAME "esp32"
#endif  // ESP32

#define IR_LED D3  
//IRsend irsend(IR_LED);  // Set the GPIO to be used to sending the message.
IRHitachiAc296 irhi(IR_LED);

uint8_t currentTemp = 25;
uint8_t currentFan = kHitachiAc296FanMedium;
uint8_t currentMode = kHitachiAc296Cool;

//auto fan, heating, & 24° Celsius

void handleRoot() {
  //const std::string *index_html = R"rawliteral(
  char index_head[] = R"rawliteral(
  <!DOCTYPE HTML><html><head>
    <title>AC Controller</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    </head><body>
    <form action="/get">
      input1: <input type="text" name="input1">
      <input type="submit" value="Submit">
    </form><br>
    <form action="/get">
      input2: <input type="text" name="input2">
      <input type="submit" value="Submit">
    </form><br>
    <form action="/get">
      input3: <input type="text" name="input3">
      <input type="submit" value="Submit">
    </form>
    <form action="/on">
      <input type="submit" value="ON" />
    </form>
    <form action="/off">
      <input type="submit" value="OFF" />
    </form>
    <form action="/tempUp">
      <input type="submit" value="+" />
    </form>
    <form action="/tempDown">
      <input type="submit" value="-" />
    </form>)rawliteral";

  char index_cntnt[2];
  itoa(irhi.getTemp(), index_cntnt, 10);

  char index_foot[] = R"rawliteral(
  </body></html>)rawliteral";

  strncat(index_head, index_cntnt, 2);
  //strncat(index_head, index_foot, 14);

  server.send(200, "text/html", index_head);
}

void handleOn() {
  irhi.on();
  sendCurrentState();
  redirectToIndex();
}

void handleOff() {
  irhi.off();
  sendCurrentState();
  redirectToIndex();
}

void handleTempUp() {
  currentTemp++;
  irhi.setTemp(currentTemp);
  sendCurrentState();
  redirectToIndex();
}

void handleTempDown() {
  currentTemp--;
  irhi.setTemp(currentTemp);
  sendCurrentState();
  redirectToIndex();
}

void redirectToIndex() {
  server.send(200, "text/html", "<html><meta http-equiv=\"refresh\" content=\"0; URL=/\" /></html>");
}

void sendCurrentState() {
  irhi.send(0);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  
  server.send(404, "text/plain", message);
}

void disableBuiltinLed() {
  digitalWrite(LED_BUILTIN, HIGH);
  digitalWrite(D4, HIGH);
  pinMode(D4, OUTPUT);
}

void initWifi() {
  WiFi.begin(kSsid, kPassword);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(kSsid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP().toString());

#if defined(ESP8266)
  if (mdns.begin(HOSTNAME, WiFi.localIP())) {
#else  // ESP8266
  if (mdns.begin(HOSTNAME)) {
#endif  // ESP8266
    Serial.println("MDNS responder started");
    // Announce http tcp service on port 80
    mdns.addService("http", "tcp", 80);
  }
}

void initServer() {
  server.on("/", handleRoot);
  server.on("/on", handleOn);
  server.on("/off", handleOff);
  server.on("/tempUp", handleTempUp);
  server.on("/tempDown", handleTempDown);
  
  server.onNotFound(handleNotFound);

  server.begin();
}

void initIrhi() {
  pinMode(IR_LED, OUTPUT);

  //irsend.begin();
  irhi.begin();

  irhi.setTemp(currentTemp);
  irhi.setFan(currentFan);
  irhi.setMode(currentMode);
}

void setup(void) {
  Serial.begin(115200);
  Serial.println("");
  
  disableBuiltinLed();
  initWifi();
  initServer();
  initIrhi();
  Serial.println("HTTP server started");
}

void loop(void) {
#if defined(ESP8266)
  mdns.update();
#endif
  
  server.handleClient();
}