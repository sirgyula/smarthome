void handleRoot();
void handleOn();
void handleOff();
void handleTempUp();
void handleTempDown();
void handleNotFound();

void redirectToIndex();
void sendCurrentState();

void initWifi();
void initServer();
void initIrhi();

const char* kSsid = "*";
const char* kPassword = "*";
