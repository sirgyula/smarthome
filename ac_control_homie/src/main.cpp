#include <Homie.h>
#include <ir_Hitachi.h>
#include <WEMOS_SHT3X.h>
// #include <SHT3x.h>

#define firmwareVersion "1.0.0"
#define IR_LED D7

IRHitachiAc296 irhi(IR_LED);
uint8_t currentTemp = 26;
uint8_t currentFan = kHitachiAc296FanMedium;
uint8_t currentMode = kHitachiAc296Cool;

uint8_t sht30address = 0x45 ;
SHT3X sht30(sht30address);
// SHT3x Sensor;

HomieNode acSwitchNode("acswitch", "AC_switch", "switch");
HomieNode acTempNode("actemp", "AC_temp", "actempsetting");
HomieNode temperatureNode("temperature", "Temperature", "temperature");
HomieNode humidityNode("humidity", "Humidity", "humidity");

void initIrhi() {
  pinMode(IR_LED, OUTPUT);

  //irsend.begin();
  irhi.begin();

  irhi.setTemp(currentTemp);
  irhi.setFan(currentFan);
  irhi.setMode(currentMode);
}

void sendCurrentState() {
  irhi.send(0);
}

bool acSwitchHandler(const HomieRange& range, const String& value) {
  if (value != "true" && value != "false") return false;

  bool on = (value == "true");

  if (on) {
    irhi.on();
    sendCurrentState();
  } else {
    irhi.off();
    sendCurrentState();
  }

  acSwitchNode.setProperty("on").send(value);
  Homie.getLogger() << "AC is " << (on ? "on" : "off") << endl;

  return true;
}

bool acTempHandler(const HomieRange& range, const String& value) {
  currentTemp = value.toInt();
  irhi.setTemp(currentTemp);
  sendCurrentState();
  Homie.getLogger() << "temp set is " << value << endl;

  return true;
}

void temperatureHumidityLoopHandler() {
  if (sht30.get() == 0) {
    float temperature = sht30.cTemp;
    float humidity = sht30.humidity;

    Homie.getLogger() << "Temperature: " << temperature << "°C" << endl;
    Homie.getLogger() << "Humidity: " << humidity << "%" << endl;

    temperatureNode.setProperty("degrees").send(String(temperature));
    humidityNode.setProperty("humidity").send(String(humidity));
  }

  delay(10000);
}

/*void temperatureHumidityLoopHandler() {
  Sensor.UpdateData();

  float temperature = Sensor.GetTemperature();
  float humidity = Sensor.GetAbsHumidity();

  Homie.getLogger() << "Temperature: " << temperature << "°C" << endl;
  Homie.getLogger() << "Humidity: " << humidity << "%" << endl;

  temperatureNode.setProperty("degrees").send(String(temperature));
  humidityNode.setProperty("humidity").send(String(humidity));

  delay(1000);
}*/

void setup() {
  Serial.begin(115200);
  Serial << endl << endl;

  Homie_setFirmware("ac_controller_homie", firmwareVersion);

  Homie.setLoopFunction(temperatureHumidityLoopHandler);

  acSwitchNode.advertise("on").setName("On").setDatatype("boolean").settable(acSwitchHandler);
  acTempNode.advertise("t").setName("T").setDatatype("integer").settable(acTempHandler);
  temperatureNode.advertise("degrees").setName("Degrees").setDatatype("float").setUnit("ºC");
  humidityNode.advertise("humidity").setName("Humidity").setDatatype("float");

  Homie.setup();

  initIrhi();
}

void loop() {
  Homie.loop();
}
